const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const misRutas = require("./router/index");
const path = require("path");
const axios = require('axios');

const app = express();


app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
//app.use(bodyparser.urlencoded ({extended:true}));// Para paginas como la cotización
 // Cambia los js por html (Se debe cambiar el nombre a .html en la carpeta views)
//app.use(bodyparser.urlencoded({extended:true}));
app.use(express.json());
//app.use(bodyparser.json());
app.use(misRutas);
app.engine('html', require('ejs').renderFile);



app.use((req, res, next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

// Escuchar al servidor por el puerto 3000
const puerto = 3001; // Victor
app.listen(puerto, ()=>{
    console.log("iniciando puerto 3001")
})
