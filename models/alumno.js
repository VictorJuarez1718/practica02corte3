const json = require("express/lib/response");
const resolve = require("path/posix");
const promise = require("../models/conexion.js");
const conexion = require("../models/conexion.js");
const axios = require('axios');

var AlumnoDb = {}

AlumnoDb.insertar = function insertar(alumno){

    return new Promise((resolve, reject)=>{
        var sqlConsulta = "Insert into alumno set ?";
        conexion.query(sqlConsulta, alumno, function(err, res){
            if(err){
                reject(err.message);
            }else{
                resolve({
                    id:res.insertId,
                    matricula:alumno.matricula,
                    nombre:alumno.nombre,
                    domicilio:alumno.domicilio,
                    sexo:alumno.sexo,
                    especialidad:alumno.especialidad

                })
            }
        })
    })

}


AlumnoDb.mostrarTodos = function mostrarTodos(){
    alumno = {};
    return new Promise((resolve, reject)=>{
        
        var sqlConsulta = "Select * from alumno";
        conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                reject(err.message);
            }else{
                alumno = res;
                resolve(alumno)
            }
        })

    })
}

/* Buscar por matricula */
AlumnoDb.buscarMatricula = function buscarMatricula(matricula){
    alumno = {};
    return new Promise((resolve, reject)=>{
        
        var sqlConsulta = "Select * from alumno WHERE matricula = ?";
        conexion.query(sqlConsulta,[matricula],function(err,res){
            if(err){
                reject(err.message);
            }else{
                alumno = res;
                resolve(res);
            }
        })

    })
}


/* Borrar por matricula */
AlumnoDb.borrarPorMatricula = function borrarPorMatricula(matricula){
   alumno = {};
    return new Promise((resolve, reject)=>{
        
        var sqlConsulta = "DELETE from alumno WHERE matricula = ?";
        conexion.query(sqlConsulta,[matricula],function(err,res){
            if(err){
                reject(err.message);
            }else{
                console.log(res.affectedRows);
                resolve(res.affectedRows);
            }
        })

    })
}

/* Actualizar alumnos por matricula */
AlumnoDb.actualizar = function actualizar(alumno){
    //alumno = {};
    return new Promise((resolve, reject)=>{
        
        var sqlConsulta = "UPDATE alumno SET nombre = ?, domicilio = ?, sexo = ?, especialidad = ? WHERE matricula = ?";
        conexion.query(sqlConsulta,[alumno.nombre, alumno.domicilio, alumno.sexo, alumno.especialidad, alumno.matricula],function(err,res){
            if(err){
                reject(err.message);
                console.log("Error")
            }else{
                resolve(res.affectedRows);
            }
        })

    })
}



module.exports = AlumnoDb;