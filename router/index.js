//How to insert records with axios and a post request?        

const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");
const db = require("../models/alumno.js");
const axios = require('axios');

router.get("/", (req, res)=>{
  const valores = {
      matricula:req.query.matricula,
      nombre:req.query.nombre,
      domicilio:req.query.domicilio,
      sexo:req.query.sexo,
      especialidad:req.query.especialidad
  }
  res.render('index.html', valores);

})

// Método para insertar
router.post("/insertar", async (req, res) => {
  alumno = {
    matricula: req.body.matricula,
    nombre: req.body.nombre,
    domicilio: req.body.domicilio,
    sexo: req.body.sexo,
    especialidad: req.body.especialidad,
  };
  let resultado = await db.insertar(alumno)
  res.json(resultado)
})

// Método para mostrar todo
router.get('/mostrarTodos', async(req, res)=>{
  resultado = await db.mostrarTodos();
  res.json(resultado)
})

// Buscar matricula
router.get('/buscarMatricula', (req, res) =>{
  const matricula = req.query.matricula;
  db.buscarMatricula(matricula)
      .then((data) => res.json(data))
      .catch((err) => res.status(404).send(err.message))

})

// Borrar matricula
router.delete('/borrarPorMatricula', (req, res) => {
  const matricula = req.query.matricula;
  db.borrarPorMatricula(matricula)
      .then((data) => res.json(data))
      .catch((err) => res.status(404).send(err.message))
})

// Actualizar por matricula versión 2
router.put("/actualizar", async(req, res)=>{
  const alumno = req.body;
  db.actualizar(alumno)
      .then((data) => res.json(data))
      .catch((err) => res.status(404).send(err.message))
})


router.post("/", (req, res)=>{
  const valores = {
    matricula:req.query.matricula,
    nombre:req.query.nombre,
    domicilio:req.query.domicilio,
    sexo:req.query.sexo,
    especialidad:req.query.especialidad
  }
  res.render("index.html", valores);
})




module.exports = router;